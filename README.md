# README #


### What is this repository for? ###

* It provides a method which you can directly use to chatter users just by passing the respective parameters.

### How do I get set up? ###

* Place the below code in a method in apex class which will be used to send chatter to users:

~~~~
	public static void chatterUser(String message, List<String> userId, String recordId) {
        ConnectApi.FeedItemInput feedItemInput = new ConnectApi.FeedItemInput();
        ConnectApi.MentionSegmentInput mentionSegmentInput = new ConnectApi.MentionSegmentInput();
        ConnectApi.MessageBodyInput messageBodyInput = new ConnectApi.MessageBodyInput();
        ConnectApi.TextSegmentInput textSegmentInput = new ConnectApi.TextSegmentInput();

        messageBodyInput.messageSegments = new List<ConnectApi.MessageSegmentInput>();

        mentionSegmentInput.id = userId.get(0);
        messageBodyInput.messageSegments.add(mentionSegmentInput);

        textSegmentInput.text = '\n'+message;
        messageBodyInput.messageSegments.add(textSegmentInput);

        feedItemInput.body = messageBodyInput;
        feedItemInput.feedElementType = ConnectApi.FeedElementType.FeedItem;
        feedItemInput.subjectId = recordId;
		
         ConnectApi.FeedElement feedElement = ConnectApi.ChatterFeeds.postFeedElement(Network.getNetworkId(), feedItemInput);   
    }
~~~~
* Then let's say we want to send chatter to owners of Account. So, we'll write something like below in a different method and call the above method:

~~~~
	List<Account> accountRecords = new List<Account>([SELECT Id, OwnerId FROM Account LIMIT 10]);
	List<String> ownerIds = new List<String>();
	String message = 'Check your account details and please confirm asap.';
	ownerIds.add(String.valueOf(accountRecords[0].OwnerId));
	String recordId = accountRecords[0].Id;
	chatterUser(message, ownerIds, recordId);
~~~~
* This sending of chatter to Account owners is just an example, you can always use it according to your requirements.